import 'package:filter_exp/screens/password_input.dart';
import 'package:filter_exp/screens/phone_input.dart';
import 'package:filter_exp/screens/submit_button.dart';
import 'package:flutter/material.dart';

import 'auth_change_page.dart';

import 'email_input.dart';
import 'form_dimensions.dart';
import 'glassmorphism_cover.dart';

class SignPhoneForm extends StatefulWidget {
  final void Function() flipCard;

  const SignPhoneForm({
    Key? key,
    required this.flipCard,
  }) : super(key: key);

  @override
  State<SignPhoneForm> createState() => _SignPhoneFormState();
}

class _SignPhoneFormState extends State<SignPhoneForm> {
  bool inItialized = false;

  @override
  Widget build(BuildContext context) {
    return GlassMorphismCover(
      borderRadius: BorderRadius.circular(16.0),
      child: FormDimensions(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const Text(
                "Log In",
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.white,
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const AuthPhoneInput(),
                  const SizedBox(
                    height: 20,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  SubmitButton(
                    isLoading: false,
                    onSubmitted: () {},
                    buttonText: "Submit",
                  )
                ],
              ),
              Column(
                children: [
                  AuthChangePage(
                    infoText: "Flip to login with",
                    flipPageText: "Email",
                    flipCard: widget.flipCard,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}