import 'dart:ui';

import 'package:easy_dynamic_theme/easy_dynamic_theme.dart';
import 'package:filter_exp/screens/main_dairy.dart';
import 'package:filter_exp/theme/app_theme.dart';
import 'package:filter_exp/theme/dynamic_color_builder.dart';
import 'package:filter_exp/theme/theme.dart';
import 'package:flutter/material.dart';


// void main() {
//   runApp(const MyApp());
// }

/*class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        useMaterial3: true,

        primarySwatch: Colors.blue,
      ),
      home:  MyFrosted()
    );
  }
}

class MyFrosted extends StatefulWidget {


  @override
  State<MyFrosted> createState() => _MyFrostedPageState();
}

class _MyFrostedPageState extends State<MyFrosted> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/intro2.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: ClipRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                child: Container(
                  width: 300.0,
                  height: 300.0,
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                      //color: Colors.grey.shade200.withOpacity(0.5)),
                      color: Colors.white.withOpacity(0.1)),
                  child: Center(
                    child: Text('Frosted',
                        style: Theme.of(context).textTheme.displayMedium),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}*/

// void main() {
//   runApp(const MyApp());
// }

void main() async {
  runApp(
    EasyDynamicThemeWidget(
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    /*return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const Home());*/

    /*return DynamicColorBuilder(
        builder: (ColorScheme? lightColorScheme, ColorScheme? darkColorScheme) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            theme: AppTheme.lightTheme(lightColorScheme),
            darkTheme: AppTheme.darkTheme(darkColorScheme),
            home: const Home(),
          );
        });*/

    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: lightThemeData,
      darkTheme: darkThemeData,
      themeMode: EasyDynamicTheme.of(context).themeMode,
      home: const Home(),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => DairyaholicAuthPage(),
                    ),
                  );
                },
                child: const Text("login page"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
